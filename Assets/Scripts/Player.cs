using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public float MoveSpeed = 10;
    public float RotationSpeed = 10;

    public float MinCollisionVelocity;

    public float JoystickDeadzone = 0.4f;

    public string LeftHStick = "1LHorizontal"; 
    public string LeftVStick = "1LVertical"; 
    public string RightHStick = "1RHorizontal";
    public string RightVStick = "1RVertical";

    // Use this for initialization
    void Start()
    {
        foreach (Destroyable d in transform.GetComponentsInChildren<Destroyable>())
        {
            d.player = this;
            Debug.Log(d.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // movement
        Vector2 leftStick = new Vector2(Input.GetAxis(LeftHStick), Input.GetAxis(LeftVStick));
       
        if (leftStick.magnitude < JoystickDeadzone)
        {
            leftStick = Vector2.zero;
        }
        else // scale the input such that the deadzone doesn't only cut off. ref: http://www.gamasutra.com/blogs/JoshSutphin/20130416/190541/Doing_Thumbstick_Dead_Zones_Right.php
        {
            leftStick.y *= -1;
            leftStick = leftStick.normalized * ((leftStick.magnitude - JoystickDeadzone) / (1 - JoystickDeadzone));
        }

        //transform.position = Vector3.Lerp(transform.position, 
        //    transform.position + new Vector3(leftStick.x, 0, leftStick.y) * MoveSpeed, 
        //    Time.deltaTime);
        rigidbody.AddForce(new Vector3(leftStick.x, 0, leftStick.y).normalized * MoveSpeed, ForceMode.Acceleration);

        // rotation
        Vector2 rightStick = new Vector2(Input.GetAxis(RightHStick), Input.GetAxis(RightVStick));
        if (rightStick.magnitude < 0.3f)
        {
            rightStick = Vector2.zero;
        }
        else
        {
            rightStick.y *= -1;
            rightStick = rightStick.normalized * ((rightStick.magnitude - JoystickDeadzone) / (1 - JoystickDeadzone));
        }

        if (rightStick != Vector2.zero)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.LookRotation(new Vector3(rightStick.x, 0, rightStick.y)), Time.deltaTime * RotationSpeed);
        }
        else
        {
            rigidbody.angularVelocity = Vector3.zero;
        }
    }
}
