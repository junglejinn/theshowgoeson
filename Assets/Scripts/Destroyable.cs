using UnityEngine;
using System.Collections;

public class Destroyable : MonoBehaviour
{
    public float Health = 10;
    public float HitPoints = 4;
    public static float KickoffForce = 100;

    public Transform Blood;

    public Player player = null;

    // Use this for initialization
    void Start()
    {
        Blood.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FallOff(Vector3 hitVelocity)
    {
        Blood.gameObject.SetActive(true);

        Transform t = transform.FindChild(name);
        t.gameObject.SetActive(true);
        t.parent = null;
        t.rigidbody.AddForce(hitVelocity.normalized * KickoffForce);
    }

    void OnTriggerEnter(Collider other)
    {
        Destroyable d;
        if ((d = other.gameObject.GetComponent<Destroyable>()) != null)
        {
            //Debug.Log(0);
            //if (d.player != null && player != null)
            //{
            //    Debug.Log(1);
            //    if (d.player.name == player.name) return; // don't hurt yourself

            //    // only hurt the other if crashing together fast enough
            //    float relativeVelocity = (d.player.rigidbody.velocity.magnitude + player.rigidbody.velocity.magnitude) * 0.5f;
            //    Debug.Log(relativeVelocity);
            //    if (relativeVelocity < player.MinCollisionVelocity)
            //    {
            //        return;
            //    }
                
            //}

            d.Health -= HitPoints;
            d.renderer.material.color += Color.red * 0.4f;

            if (d.Health < 0)
            {
                d.FallOff(new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)).normalized);
                renderer.enabled = false;
                Destroy(this);
                Destroy(d);
            }
        }
    }
}
