using UnityEngine;
using System.Collections;

public class Couple : MonoBehaviour
{
    public float RotationSpeed = 10;
    Transform[] children;

    // Use this for initialization
    void Start()
    {
        children = transform.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 center = transform.position;

        for (int i = 0; i < children.Length; i++)
        {
            center += children[i].position;
        }
        center /= children.Length;

        transform.RotateAround(center, Vector3.up, RotationSpeed * Time.deltaTime);
    }
}
