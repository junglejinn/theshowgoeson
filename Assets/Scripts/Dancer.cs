using UnityEngine;
using System.Collections;

public class Dancer : MonoBehaviour
{
    private int attachedLimbs;

    // Use this for initialization
    void Start()
    {
        attachedLimbs = transform.GetChildCount();
    }

    public void DetachLimb()
    {
        attachedLimbs--;
        renderer.material.color += Color.red * 0.25f;

        //SpringJoint j = transform.parent.GetComponentInChildren<SpringJoint>();
        //if (j)
        //{
        //    j.connectedBody = null;
        //    Destroy(j);
        //}

        if (attachedLimbs <= 0)
        {
            rigidbody.constraints = RigidbodyConstraints.None;
            transform.parent = null;
            Destroy(this);
        }
    }
}
